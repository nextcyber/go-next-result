package res

// Error Error接口
type Error interface {

	// GetCode 获取状态码
	GetCode() int

	// SetCode 设置状态码
	//
	// @param code 状态码
	SetCode(code int)

	// GetMsg 获取异常信息
	GetMsg() string

	// SetMsg 设置异常信息
	//
	// @param msg 异常信息
	SetMsg(msg string)
}
