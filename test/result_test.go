package test_test

import (
	"encoding/json"
	"fmt"
	"gitee.com/nextcyber/go-next-result/res"
	"testing"
)

func TestOk(t *testing.T) {
	_result := res.NewMultipleWithEmpty[string, string]()
	_result.SetUid("123")
	_result.Put("user", "admin")
	_result.Put("password", "123456")
	fmt.Println(_result.GetUid())
	fmt.Println(_result)

	// data测试
	var _data res.Data[map[string]any]
	_data = res.NewMultipleWithOne[string, any]("count", 1)
	fmt.Println(_data)

	_pr := res.NewPlainWithData("Hello world!!!")
	fmt.Println(_pr)
}

func TestFailure(t *testing.T) {
	_mr := res.NewMultipleFailureWithMsg[string, any](500355, "无权限访问，请联系管理员!!!")
	_pr := res.NewPlainFailureWithMsg[string](500458, "Token已失效，请重新登陆!!!")
	fmt.Println(_mr)
	fmt.Println(_mr.Success)
	fmt.Println(_pr)
	fmt.Println(_pr.Success)
}

func TestOKJson(t *testing.T) {
	_result := res.NewMultipleFailureWithEmpty[string, string]()
	_result.SetUid("333")
	_result.Put("user", "admin")
	_result.Put("password", "123456")
	_result.SetStatus(404)
	_data, _error := json.Marshal(_result)
	fmt.Println(_result.GetHttpStatus())
	if _error != nil {
		fmt.Printf("序列化失败: %v\n", _error)
	}
	fmt.Println(string(_data))

	_mr := res.NewPlainFailureWithMsg[string](500355, "无权限访问，请联系管理员!!!")
	_md, _err := json.Marshal(_mr)
	if _err != nil {
		fmt.Printf("序列化失败: %v\n", _err)
	}
	fmt.Println(string(_md))
}
