package res

// Response 响应接口
type Response interface {

	// GetUid 获取请求唯一ID
	GetUid() string

	// SetUid 设置唯一ID
	//
	// 唯一ID
	SetUid(uid string)

	// GetStatus 获取HTTP状态码
	GetStatus() int

	// SetStatus 设置HTTP状态码
	//
	// @param status HTTP状态码
	SetStatus(status int)

	// GetHttpStatus 获取Http状态码
	GetHttpStatus() int
}

// BaseResponse 基础响应
type BaseResponse struct {
	// Uid 唯一ID
	Uid string `json:"uid"`
	// HTTP状态码
	Status int `json:"-"`
}

func (_self *BaseResponse) GetUid() string {
	return _self.Uid
}

func (_self *BaseResponse) SetUid(uid string) {
	_self.Uid = uid
}

func (_self *BaseResponse) GetStatus() int {
	return _self.Status
}

func (_self *BaseResponse) SetStatus(status int) {
	_self.Status = status
}

func (_self *BaseResponse) GetHttpStatus() int {
	return _self.Status
}
