package res

import (
	"reflect"
)

// MultipleResult 多值结果集
type MultipleResult[K DRKey, V any] struct {
	// BaseResponse 基础响应对象
	*BaseResponse
	// Brief 简要信息对象
	*Brief
	// Map数据
	Data map[K]V `json:"data"`
}

func (_self *MultipleResult[K, V]) GetData() map[K]V {
	return _self.Data
}

func (_self *MultipleResult[K, V]) SetData(data map[K]V) {
	if data == nil {
		_self.Data = make(map[K]V, 8)
	} else {
		_self.Data = data
	}
}

func (_self *MultipleResult[K, V]) Get(key K) V {
	return _self.Data[key]
}

func (_self *MultipleResult[K, V]) Put(key K, value V) {
	_self.Data[key] = value
}

func (_self *MultipleResult[K, V]) PutIfAbsent(key K, value V) {
	if !_self.ContainsKey(key) {
		_self.Data[key] = value
	}
}

func (_self *MultipleResult[K, V]) PutAll(data map[K]V) {
	if data != nil && len(data) > 0 {
		for key, value := range data {
			_self.Data[key] = value
		}
	}
}

func (_self *MultipleResult[K, V]) PutAllIfAbsent(data map[K]V) {
	if data != nil && len(data) > 0 {
		for key, value := range data {
			if !_self.ContainsKey(key) {
				_self.Data[key] = value
			}
		}
	}
}

func (_self *MultipleResult[K, V]) Remove(key K) {
	if _self.ContainsKey(key) {
		delete(_self.Data, key)
	}
}

func (_self *MultipleResult[K, V]) Clear() {
	_self.Data = make(map[K]V, 8)
}

func (_self *MultipleResult[K, V]) ContainsKey(key K) bool {
	if _self.Data == nil {
		return false
	}
	_, hasKey := _self.Data[key]
	return hasKey
}

func (_self *MultipleResult[K, V]) ContainsValue(value V) bool {
	if !_self.IsEmpty() {
		for _, v := range _self.Data {
			if reflect.DeepEqual(v, value) {
				return true
			}
		}
	}
	return false
}

func (_self *MultipleResult[K, V]) GetSize() int {
	if _self.Data == nil {
		return 0
	}
	return len(_self.Data)
}

func (_self *MultipleResult[K, V]) IsEmpty() bool {
	return _self.Data == nil || len(_self.Data) == 0
}

// NewMultipleWithEmpty 创建实例
func NewMultipleWithEmpty[K DRKey, V any]() *MultipleResult[K, V] {
	_result := &MultipleResult[K, V]{
		BaseResponse: &BaseResponse{
			Status: 200,
		},
		Data: map[K]V{},
		Brief: &Brief{
			Code:    200,
			Success: true,
		},
	}
	_result._setTime()
	return _result
}

// NewMultipleWithSize 创建实例
//
// @param size 容量大小
func NewMultipleWithSize[K DRKey, V any](size int) *MultipleResult[K, V] {
	_result := &MultipleResult[K, V]{
		BaseResponse: &BaseResponse{
			Status: 200,
		},
		Data: make(map[K]V, size),
		Brief: &Brief{
			Code:    200,
			Success: true,
		},
	}
	_result._setTime()
	return _result
}

// NewMultipleWithOne 创建实例
//
// @param key 键
//
// @param value 值
func NewMultipleWithOne[K DRKey, V any](key K, value V) *MultipleResult[K, V] {
	_result := NewMultipleWithSize[K, V](8)
	_result.Put(key, value)
	return _result
}

// NewMultipleWithMany 创建实例
//
// @param data 多个值
func NewMultipleWithMany[K DRKey, V any](data map[K]V) *MultipleResult[K, V] {
	var _size int
	if data == nil || len(data) == 0 {
		_size = 8
	} else {
		_size = len(data)
	}
	_result := NewMultipleWithSize[K, V](_size)
	_result.PutAll(data)
	return _result
}

// NewMultipleFailure 创建实例
//
// @param status HTTP状态码
//
// @param code 状态码
//
// @param msg 错误消息
func NewMultipleFailure[K DRKey, V any](status int, code int, msg string) *MultipleResult[K, V] {
	_result := &MultipleResult[K, V]{
		BaseResponse: &BaseResponse{
			Status: status,
		},
		Data: map[K]V{},
		Brief: &Brief{
			Code:    code,
			Msg:     msg,
			Success: false,
		},
	}
	_result._setTime()
	return _result
}

// NewMultipleFailureWithEmpty 创建实例
func NewMultipleFailureWithEmpty[K DRKey, V any]() *MultipleResult[K, V] {
	return NewMultipleFailure[K, V](200, 500500, "Server Error")
}

// NewMultipleFailureWithMsg 创建实例
//
// @param code 状态码
//
// @param msg 错误信息
func NewMultipleFailureWithMsg[K DRKey, V any](code int, msg string) *MultipleResult[K, V] {
	return NewMultipleFailure[K, V](200, code, msg)
}
