package res

// PlainResult 单值结果集
type PlainResult[T any] struct {
	// BaseResponse 基础响应对象
	*BaseResponse
	// Brief 简要信息对象
	*Brief
	// Data 数据
	Data T `json:"data"`
}

func (_self *PlainResult[T]) GetData() T {
	return _self.Data
}

func (_self *PlainResult[T]) SetData(data T) {
	_self.Data = data
}

// NewPlainWithEmpty 创建实例
func NewPlainWithEmpty[T any]() *PlainResult[T] {
	_result := &PlainResult[T]{
		BaseResponse: &BaseResponse{
			Status: 200,
		},
		Brief: &Brief{
			Code:    200,
			Success: true,
		},
	}
	_result._setTime()
	return _result
}

// NewPlainWithData 创建实例
//
// @param data 数据
func NewPlainWithData[T any](data T) *PlainResult[T] {
	_result := &PlainResult[T]{
		BaseResponse: &BaseResponse{
			Status: 200,
		},
		Data: data,
		Brief: &Brief{
			Code:    200,
			Success: true,
		},
	}
	_result._setTime()
	return _result
}

// NewPlainFailure 创建实例
//
// @param status HTTP状态值
//
// @param code 状态码
//
// @param msg 错误信息
func NewPlainFailure[T any](status int, code int, msg string) *PlainResult[T] {
	_result := &PlainResult[T]{
		BaseResponse: &BaseResponse{
			Status: status,
		},
		Brief: &Brief{
			Code:    code,
			Msg:     msg,
			Success: false,
		},
	}
	_result._setTime()
	return _result
}

// NewPlainFailureWithEmpty 创建实例
func NewPlainFailureWithEmpty[T any]() *PlainResult[T] {
	return NewPlainFailure[T](200, 500500, "Server Error")
}

// NewPlainFailureWithMsg 创建实例
//
// @param code 状态码
//
// @param msg 错误信息
func NewPlainFailureWithMsg[T any](code int, msg string) *PlainResult[T] {
	return NewPlainFailure[T](200, code, msg)
}
