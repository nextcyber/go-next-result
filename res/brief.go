package res

import (
	"strconv"
	"time"
)

// Brief 异常简要信息类
type Brief struct {
	// Success 是否成功
	Success bool `json:"success"`
	// 状态码
	Code int `json:"code"`
	// Msg 异常信息
	Msg string `json:"msg"`
	// Timestamp 时间戳
	Timestamp string `json:"timestamp"`
	// GmtCreate 时间字符串
	GmtCreate string `json:"gmtCreate"`
}

func (_self *Brief) GetCode() int {
	return _self.Code
}

func (_self *Brief) SetCode(code int) {
	_self.Code = code
}

func (_self *Brief) GetMsg() string {
	return _self.Msg
}

func (_self *Brief) SetMsg(msg string) {
	_self.Msg = msg
}

// Verify 检验是否成功
func (_self *Brief) Verify() {
	_self.Success = _self.Code == 0 || _self.Code == 200
}

// _setTime 设置时间
func (_self *Brief) _setTime() {
	current := time.Now()
	_self.GmtCreate = current.Format("2006-01-02 15:04:05.000")
	_self.Timestamp = strconv.FormatInt(current.UnixMilli(), 0x0a)
}
